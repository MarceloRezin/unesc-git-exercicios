const express = require("express");
const app = express();
const fs = require('fs');

const informacoesJogador = JSON.parse(fs.readFileSync("gerador_jogador.json", "utf8"));

function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

app.set("json replacer", (chave, valor) => {
    if(chave === "idade"){
        if(valor < 23){
            return "novato";
        }else if(valor < 29){
            return "profissional";
        }else if(valor < 35){
            return "veterano";
        }else{
            return "master";
        }
    }else{
        return valor;  
    }
});

app.get("/gerador", (req, res) => {
    res.send(`${informacoesJogador.nome[getRandom(0, informacoesJogador.nome.length - 1)]} ${informacoesJogador.sobrenome[getRandom(0, informacoesJogador.sobrenome.length - 1)]} é um futebolista brasileiro de ${getRandom(17, 40)} anos que atua como ${informacoesJogador.posicao[getRandom(0, informacoesJogador.posicao.length - 1)]}. Atualmente defende o ${informacoesJogador.clube[getRandom(0, informacoesJogador.clube.length - 1)]}.`);
});

app.get("/jogador", (req, res) => {

    const jogador = {
        nome: informacoesJogador.nome[getRandom(0, informacoesJogador.nome.length - 1)] + " " + informacoesJogador.sobrenome[getRandom(0, informacoesJogador.sobrenome.length - 1)],
        idade: getRandom(17, 40),
        posicao: informacoesJogador.posicao[getRandom(0, informacoesJogador.posicao.length - 1)],
        clube: informacoesJogador.clube[getRandom(0, informacoesJogador.clube.length - 1)]
    };

    res.json(jogador);
});

app.listen(3000, () => {
    console.log('Aplicação executando na porta 3000');
});