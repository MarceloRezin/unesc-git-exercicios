import React from 'react';
import Axios from 'axios';
import ComputadorForm from './ComputadorForm';

export default class Computador extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      computadores: []
    };

  }

  componentDidMount = () => {
    this.handleGet();
  }

  handleGet = () => {
    Axios.get("http://localhost:3001/computadores").then((resp) => {
      this.setState({
        computadores: resp.data
      });
    });
  }

  handlePost = (computador) => {
    Axios.post("http://localhost:3001/computadores", computador).then((resp) => {
      this.setState({
        computadores: [...this.state.computadores, computador]
      });
    });
    
  }

  handleDelete = (index) => {
    Axios.delete(`http://localhost:3001/computadores/${index}`).then((resp) => {
      this.setState({
        computadores: this.state.computadores.filter((c, i) => i !== index)
      });
    });
  }

  render(){

    let computadores = this.state.computadores.map((c, index) => {
      return (
        <div>
          <p>
            Hostname: {c.hostname} 
          </p>
          <p>
            Processador: {c.processador}
          </p>
          <p>
            Memoria: {c.memoria}
          </p>
          <p>
            Armazenamento: {c.armazenamento}
          </p>
          <p>
            Estado: {c.estado}
          </p>
          <p>
            <button onClick={() => {this.handleDelete(index)}}>Excluir</button>
          </p>
          <hr></hr>
        </div>
      );
    });

    return(
    <div>
      <h1>Computadores:</h1>
      {computadores}
      <ComputadorForm handleSubmit={this.handlePost}></ComputadorForm>
    </div>
    );
  }

}
