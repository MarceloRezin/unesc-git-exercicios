const fs = require('fs');

const NOME_ARQUIVO = "palavras.json";

var palavrasExistentes = null;
try{
    palavrasExistentes = fs.readFileSync(NOME_ARQUIVO, "utf8");
}catch(err){}

if(palavrasExistentes != null){
    palavrasExistentes = JSON.parse(palavrasExistentes);
}else{
    palavrasExistentes = [];
}

const palavrasNovas = process.argv.slice(2);

let palavrasNaoRepetidas = new Set([...palavrasExistentes, ...palavrasNovas]);
palavrasNaoRepetidas = [...palavrasNaoRepetidas].filter(p => p != null && p.trim().length > 0);

fs.writeFileSync(NOME_ARQUIVO, JSON.stringify(palavrasNaoRepetidas), { encoding: "utf8", flag: "w"});

console.log("Lista de palavras atualizadas com sucesso!");
