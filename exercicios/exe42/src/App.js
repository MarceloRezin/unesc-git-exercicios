import React from 'react';
import Temperatura from './components/Temperatura';
import Modo from './components/Modo';
import Velocidade from './components/Velocidade';

function App() {
  return (
    <div>
      <Temperatura></Temperatura>
      <Modo></Modo>
      <Velocidade></Velocidade>
    </div>
  );
}

export default App;
