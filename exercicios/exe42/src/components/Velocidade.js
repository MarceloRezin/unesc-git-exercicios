import React from 'react';

export default class Velocidade extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      velocidades: ["▁", "▁ ▅", "▁ ▅ █"],
      indexVelocidade: 0,
      velocidade: "▁"
    };

  }

  alterarVelocidade = () => {
    let novaVelocidade = this.state.indexVelocidade;
    if(novaVelocidade === 2){
      novaVelocidade = 0;
    }else{
      novaVelocidade++;
    }

    this.setState({
      indexVelocidade: novaVelocidade,
      velocidade: this.state.velocidades[novaVelocidade]
    });
  }

  render(){
    return(
    <div>
      Velocidade: {this.state.velocidade}<br />
      <button onClick={this.alterarVelocidade}>Velocidade</button>
    </div>
    );
  }

}
