Definição:

Reescreva as operações da lista abaixo usando o operador rest/spread, usandono máximo duas linhas de código:

var admin = lista[2]
admin.nome = "root"
admin.status = true
var user_a = lista[0]
user_a.status = true
var user_b = lista[1]
user_b.status = true


Use esta lista como referência:
var lista = [
	{"nome": "ramon","senha": "as54d6as5"},
	{"nome": "ronaldo","senha": "1231ds"},
	{"nome": "adm","senha": "DSF#@$"}
]

