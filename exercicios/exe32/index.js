const express = require("express");
const url = require('url');
var cookieParser = require("cookie-parser");
const app = express();

app.use(express.urlencoded({extended: true}));
app.use(cookieParser());

app.use((req, res, next) => {   
    if(req.path == "/sucesso.html" && (!req.cookies || req.cookies.logado != "true")){
        res.redirect(url.format({
            pathname:"/",
            query: {
              error: true,
              message: "Erro: Você não tem permissão para acessar essa página."
             }
        }));
    }else{
        next();
    }
});

app.use(express.static("public"));

app.get("/", (req, res) => {    
    res.redirect(url.format({
        pathname:"login.html",
        query: req.query
    }));
});

app.post("/login", (req, res) => {
    if(req.body.usuario === "root" && req.body.senha === "unesc2019"){
        res.cookie('logado', 'true');
        res.redirect("sucesso.html");
    }else{
        res.redirect(url.format({
            pathname:"/",
            query: {
              error: true,
              message: "Erro: Usuário ou senha inválidos."
             }
        }));
    }
});

app.get("/logout", (req, res) => {
    res.cookie('logado', 'false');
    res.redirect("/");
});

app.use((req, res, next) => {
    res.status(404).redirect('404.html')
});

app.listen(3000, () => {
    console.log('Aplicação executando na porta 3000');
});