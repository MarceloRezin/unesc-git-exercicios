Definição:

Crie uma pokédex contendo uma objeto com pelo menos 10 pokémons. O objeto deve conter número, nome, tipo e descrição e uma URL (local ou remota) com a imagem do pokémon. O usuário pode excluir, modificar ou inserir novos pokémons(com todas as suas informações). Ao final, deve-se apresentar um botão Showcase que exibe imagem e nome dos pokémons a cada 3 segundos. Utilize CSS para estilizar o projeto à sua maneira =)
