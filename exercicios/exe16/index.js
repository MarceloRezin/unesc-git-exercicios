function soma(numeros) {
    return numeros.reduce((acc, n) => acc + n, 0);
}