Definição:

Reescreva a função abaixo usando o método map ou reduce e arrow functions

function soma(numeros) {
	var resultado = 0;

	for(var i = 0; i < numeros.length; i++){
		resultado += numeros[i]    
	}
	
	return resultado;
}

