Definição:

Utilizando como base o exercício 04, reestruture o script para que os erros sejam tratados com instruções try-catch. O script deve implementar uma função chamada checar Personagem, e retornar um erro caso não passe pela validação.
