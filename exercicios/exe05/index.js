var total = 0;

function add(event){
    event.preventDefault();

    let valor = parseFloat(document.getElementById("valor").value);
    if(valor != NaN){
        total += valor;
    }

    let div = document.getElementById("total");
    div.innerText = total;

    if(total > 50){
        div.style.color = "red";
    }
}