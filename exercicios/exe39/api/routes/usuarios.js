const express = require("express");
const router = express.Router();
const evento = require("../data/evento");

router.get("/", (req, res) => {   
    res.json(evento.getEvento().usuarios);
});

router.get("/:id", (req, res) => {

    let usuariosComId = evento.getEvento().usuarios.filter(u => u._id == req.params.id);

    if(usuariosComId.length < 1){
        res.json({});
    }else{
        res.json(usuariosComId[0]);
    }
});

router.post("/", (req, res) => {
    const e = evento.getEvento();
    const newUsuario = req.body;  

    e.usuarios.push(newUsuario);
    evento.saveEvento(e);

    res.json(newUsuario);
});

router.put('/:id', (req, res) => {
    const e = evento.getEvento();
    const updateUsuario = req.body;

    let pos = e.usuarios.findIndex( u => u._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        e.usuarios[pos] = updateUsuario;
        evento.saveEvento(e);
        res.json(updateUsuario);
    }
});

router.delete('/:id', (req, res) => {
    const e = evento.getEvento();
    let pos = e.usuarios.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.usuarios[pos];
        e.usuarios.splice(pos, 1);

        evento.saveEvento(e);
        res.json(anterior);
    }
});

module.exports = router;