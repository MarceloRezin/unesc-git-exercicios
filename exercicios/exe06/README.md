Definição:

Crie uma página que permita ao usuário gerenciar uma lista com filmes/jogos. O formulário de inserção deve conter nome, estilo e URL da foto (local ou remota).Cada filme/jogo adicionado deve estar dentro de uma div, e deve apresentar os dados de entrada de forma estilizada (Ex.: a foto deve ser exibida com a tag img).
