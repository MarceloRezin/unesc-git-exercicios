Definição:

Crie uma página que seja capaz de gerar novas promises e imprimir na tela umalista com as promises iniciadas. Cada promise representa uma partida entre doistimes de futebol digitados pelo usuário. Ao final do tempo de 4.5 segundos, o sistema atualiza o resultado da partida (aleatoriamente) na tela. Várias promises podem ser iniciadas simultaneamente.

