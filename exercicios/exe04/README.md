Definição:

Crie um script na linguagem javascript para o envio do personagem. Os dados do formulário só devem ser enviados ao servidor caso sigam as seguintes regras:
	O nome do personagem deve conter pelo menos 3 letras
	O personagem não pode iniciar com as palavras:Admin, GM, Moderador
	É proibido elfos em venore e anões em carlin
Mensagens devem ser mostradas para o usuário informando sobre os erros.
