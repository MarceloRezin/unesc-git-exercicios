const fs = require('fs');
const NOME_ARQUIVO = "lista.json";

const listar = () => {
    var lista = null;
    try{
        lista = fs.readFileSync(NOME_ARQUIVO, "utf8");
    }catch(err){}

    if(lista != null){
        lista = JSON.parse(lista);
    }else{
        lista = [];
    }

    return lista;
};

const adicionar = (novoProduto) => {
    let lista = listar();
    lista.push(novoProduto);
    
    fs.writeFileSync(NOME_ARQUIVO, JSON.stringify(lista), { encoding: "utf8", flag: "w"});
};

module.exports = {listar, adicionar};