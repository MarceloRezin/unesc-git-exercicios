function generateOption(text){
    const op = document.createElement("option");
    op.value = text;
    op.innerText = text;

    return op;
}

const anos = document.getElementById("anos");
for(let i=1990; i<2000;  i++){
    anos.appendChild(generateOption(i));
}


const meses = document.getElementById("meses");
["Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto"]
.forEach( m => meses.appendChild(generateOption(m)));

const dias = document.getElementById("dias");
for(let i=5; i<26; i++){
    dias.appendChild(generateOption(i < 10 ? "0" + i : i));
}


