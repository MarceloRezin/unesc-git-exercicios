var lastIndex = 0;
var edicaoAtual = null;

function getForm(){
    return {
        nome: document.getElementById("nome"),
        estilo: document.getElementById("estilo"),
        url: document.getElementById("url")
    };
}

function resetForm(form){
    for(var campo in form){
        form[campo].value = "";
    }
}

function createItem(id, nome, estilo, url){
    let div = document.createElement("div");
    div.id = `item${id}`;

    let h2 = document.createElement("h2");
    h2.innerText = `Nome: ${nome}`;
    div.appendChild(h2);

    let p = document.createElement("p");
    p.innerText = `Estilo: ${estilo}`;
    div.appendChild(p);

    let img = document.createElement("img");
    img.src = url;
    img.style.height = "300";
    div.appendChild(img);

    let p2 = document.createElement("p");
    div.appendChild(p2);

    let btnEdit = document.createElement("button");
    btnEdit.innerText = "Editar";
    btnEdit.setAttribute("onclick", `updateItem(${id})`);
    p2.appendChild(btnEdit);

    let btnDelete = document.createElement("button");
    btnDelete.innerText = "Remover";
    btnDelete.setAttribute("onclick", `deleteItem(${id})`);
    p2.appendChild(btnDelete);

    div.appendChild(document.createElement("hr"));

    if(edicaoAtual != null){
        document.body.replaceChild(div, document.getElementById(`item${edicaoAtual}`));
        edicaoAtual = null;
        document.getElementById("submit").innerText = "Adicionar";
    }else{
        document.body.appendChild(div);
        lastIndex++;
    }
}

function updateItem(id){
    const div = document.getElementById(`item${id}`);

    const nomeAtual = div.childNodes[0].innerText.replace("Nome: ", "");
    const estiloAtual = div.childNodes[1].innerText.replace("Estilo: ", "");
    const urlAtual = div.childNodes[2].src;

    const form = getForm();

    form.nome.value = nomeAtual;
    form.estilo.value = estiloAtual;
    form.url.value = urlAtual;

    edicaoAtual = id;
    document.getElementById("submit").innerText = "Editar";
}

function deleteItem(id){
    document.body.removeChild(document.getElementById(`item${id}`));
}

function submit(){
    const form = getForm();
    createItem(edicaoAtual != null ? edicaoAtual : lastIndex, form.nome.value, form.estilo.value, form.url.value);
    resetForm(form);
}